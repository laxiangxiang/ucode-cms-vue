/*
 *
 * Copyright (c) 2020-2022, Java知识图谱 (http://www.altitude.xin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package xin.altitude.cms.common.util;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.core.metadata.TableInfoHelper;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * MybatisPlus增强工具类
 *
 * @author <a href="http://www.altitude.xin" target="_blank">Java知识图谱</a>
 * @author <a href="https://gitee.com/decsa/ucode-cms-vue" target="_blank">UCode CMS</a>
 * @author <a href="https://space.bilibili.com/1936685014" target="_blank">B站视频</a>
 **/
public class PlusUtils {
    @SuppressWarnings("unchecked")
    public static <E, R extends Serializable> R pkVal(TableInfo tableInfo, E e) {
        String keyProperty = tableInfo.getKeyProperty();
        return (R) tableInfo.getPropertyValue(e, keyProperty);
    }

    public static <T> Serializable pkVal(T entity, Class<T> clazz) {
        TableInfo tableInfo = TableInfoHelper.getTableInfo(clazz);
        return pkVal(tableInfo, entity);
    }

    /**
     * 用实体类全名称+实体主键ID作为Key
     *
     * @param clazz DO实体类Class对象
     * @param id    主键ID
     * @param <T>   DO实体类类型
     * @return DO实体类实例
     */
    public static <T> String customKey(Class<T> clazz, Serializable id) {
        return String.format("%s:%s", clazz.getName(), id);
    }

    public static <T> String customKey(T entity, Class<T> clazz) {
        Serializable pkVal = pkVal(entity, clazz);
        return customKey(clazz, pkVal);
    }

    /**
     * <p>增强MybatisPlus主键查询方法{@link com.baomidou.mybatisplus.core.mapper.BaseMapper#selectById(Serializable)}</p>
     * <p>增强MybatisPlus主键查询方法{@link com.baomidou.mybatisplus.extension.service.IService#getById(Serializable)}</p>
     * <p>使其具备Redis数据缓存功能 提高数据访问效率</p>
     * 普通无缓存查询数据库
     * <pre>
     *     User user = baseMapper.selectById(id);
     *     User user = service.getById(id);
     * </pre>
     * 使用缓存查询数据库
     * <pre>
     *     User user = PlusUtils.ifNotCache(id, User.class, baseMapper::selectById);
     *     User user = PlusUtils.ifNotCache(id, User.class, service::getById);
     * </pre>
     *
     * @param id     主键查询ID
     * @param clazz  DO实体类Class对象
     * @param action 访问数据库的回调
     * @param <E>    主键ID的数据类型
     * @param <T>    DO实体类数据类型
     * @return DO实体类实例
     */
    public static <E extends Serializable, T> T ifNotCache(E id, Class<T> clazz, Function<E, T> action) {
        String key = customKey(clazz, id);
        T value = RedisUtils.getObject(key, clazz);
        if (value == null) {
            T newValue = action.apply(id);
            RedisUtils.save(key, newValue);
            return newValue;
        }
        return value;
    }

    /**
     * <p>增强MybatisPlus主键查询方法{@link com.baomidou.mybatisplus.core.mapper.BaseMapper#selectById(Serializable)}</p>
     * <p>增强MybatisPlus主键查询方法{@link com.baomidou.mybatisplus.extension.service.IService#getById(Serializable)}</p>
     * <p>使其具备Redis数据缓存功能 提高数据访问效率</p>
     * 普通无缓存查询数据库
     * <pre>
     *     User user = baseMapper.selectById(id);
     *     User user = service.getById(id);
     * </pre>
     * 使用缓存查询数据库
     * <pre>
     *     User user = PlusUtils.ifNotCache(id, User.class, baseMapper::selectById);
     *     User user = PlusUtils.ifNotCache(id, User.class, service::getById);
     * </pre>
     *
     * @param id     主键查询ID
     * @param clazz  DO实体类Class对象
     * @param ms     过期时间 毫秒
     * @param action 访问数据库的回调
     * @param <E>    主键ID的数据类型
     * @param <T>    DO实体类数据类型
     * @return DO实体类实例
     */
    public static <E extends Serializable, T> T ifNotCache(E id, Class<T> clazz, long ms, Function<E, T> action) {
        String key = customKey(clazz, id);
        T value = RedisUtils.getObject(key, clazz);
        if (value == null) {
            T newValue = action.apply(id);
            RedisUtils.save(key, newValue, ms, TimeUnit.MILLISECONDS);
            return newValue;
        }
        return value;
    }

    /**
     * <p>增强MybatisPlus主键查询方法{@link com.baomidou.mybatisplus.core.mapper.BaseMapper#selectBatchIds(Collection)}</p>
     * <p>增强MybatisPlus主键查询方法{@link com.baomidou.mybatisplus.extension.service.IService#listByIds(Collection)}</p>
     * <p>使其具备Redis数据缓存功能 提高数据访问效率</p>
     * 普通无缓存查询数据库
     * <pre>
     *     User[] users = baseMapper.selectBatchIds(ids);
     *     User[] users = service.listByIds(ids);
     * </pre>
     * 使用缓存查询数据库
     * <pre>
     *     User[] users = PlusUtils.ifNotCache(ids, User.class, baseMapper::selectBatchIds);
     *     User[] users = PlusUtils.ifNotCache(ids, User.class, service::listByIds);
     * </pre>
     *
     * @param ids    批主键查询ID
     * @param clazz  DO实体类Class对象
     * @param action 访问数据库的回调
     * @param <T>    DO实体类数据类型
     * @return DO实体类实例
     */
    public static <T> List<T> ifNotCache(Collection<? extends Serializable> ids, Class<T> clazz, Function<Collection<? extends Serializable>, ? extends List<T>> action) {
        List<String> keys = EntityUtils.toList(ids, e -> customKey(clazz, e));
        List<T> ts = EntityUtils.toList(RedisUtils.multiGet(keys), e -> JacksonUtils.readObjectValue(e, clazz), Objects::nonNull);
        if (ts.size() < ids.size()) {
            TableInfo tableInfo = TableInfoHelper.getTableInfo(clazz);
            if (tableInfo == null) {
                throw new RuntimeException("当前实体类不属于DO层");
            }
            List<T> newValue = action.apply(ids);
            Map<Serializable, T> map = EntityUtils.toMap(newValue, e -> pkVal(tableInfo, e), Function.identity());
            RedisUtils.multiSet(MapUtils.transMap(map, e -> customKey(clazz, e), JacksonUtils::writeValue));
            return newValue;
        }
        return ts;
    }

    /**
     * <p>增强MybatisPlus主键查询方法{@link com.baomidou.mybatisplus.core.mapper.BaseMapper#selectBatchIds(Collection)}</p>
     * <p>增强MybatisPlus主键查询方法{@link com.baomidou.mybatisplus.extension.service.IService#listByIds(Collection)}</p>
     * <p>使其具备Redis数据缓存功能 提高数据访问效率</p>
     * 普通无缓存查询数据库
     * <pre>
     *     User[] users = baseMapper.selectBatchIds(ids);
     *     User[] users = service.listByIds(ids);
     * </pre>
     * 使用缓存查询数据库
     * <pre>
     *     User[] users = PlusUtils.ifNotCache(ids, User.class, baseMapper::selectBatchIds);
     *     User[] users = PlusUtils.ifNotCache(ids, User.class, service::listByIds);
     * </pre>
     *
     * @param ids    批主键查询ID
     * @param clazz  DO实体类Class对象
     * @param ms     过期时间 毫秒
     * @param action 访问数据库的回调
     * @param <T>    DO实体类数据类型
     * @return DO实体类实例
     */
    public static <T> List<T> ifNotCache(Collection<? extends Serializable> ids, Class<T> clazz, long ms, Function<Collection<? extends Serializable>, ? extends List<T>> action) {
        List<String> keys = EntityUtils.toList(ids, e -> customKey(clazz, e));
        List<T> ts = EntityUtils.toList(RedisUtils.multiGet(keys), e -> JacksonUtils.readObjectValue(e, clazz), Objects::nonNull);
        if (ts.size() < ids.size()) {
            TableInfo tableInfo = TableInfoHelper.getTableInfo(clazz);
            List<T> newValue = action.apply(ids);
            Map<Serializable, T> map = EntityUtils.toMap(newValue, e -> pkVal(tableInfo, e), Function.identity());
            Map<String, String> transMap = MapUtils.transMap(map, e -> customKey(clazz, e), JacksonUtils::writeValue);
            transMap.forEach((k, v) -> RedisUtils.save(k, v, ms, TimeUnit.MILLISECONDS));
            return newValue;
        }
        return ts;
    }


}
