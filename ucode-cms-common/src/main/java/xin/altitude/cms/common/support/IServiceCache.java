/*
 *
 * Copyright (c) 2020-2022, Java知识图谱 (http://www.altitude.xin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package xin.altitude.cms.common.support;

import com.baomidou.mybatisplus.extension.service.IService;
import xin.altitude.cms.common.util.EntityUtils;
import xin.altitude.cms.common.util.PlusUtils;
import xin.altitude.cms.common.util.RedisUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * 增强{@link IService} 用于实现Redis分布式缓存功能
 *
 * @author <a href="http://www.altitude.xin" target="_blank">Java知识图谱</a>
 * @author <a href="https://gitee.com/decsa/ucode-cms-vue" target="_blank">UCode CMS</a>
 * @author <a href="https://space.bilibili.com/1936685014" target="_blank">B站视频</a>
 **/
public interface IServiceCache<T> extends IService<T> {
    /**
     * 重载{@link IService#removeById(Serializable)}方法 增加Redis分布式缓存功能
     *
     * @param id    主键ID
     * @param clazz {@code T}实体类Class对象
     * @return true表示删除成功
     */
    default boolean removeById(Serializable id, Class<T> clazz) {
        String key = PlusUtils.customKey(clazz, id);
        boolean result = removeById(id);
        RedisUtils.remove(key);
        return result;
    }

    /**
     * 重载{@link IService#removeById(Object)}方法 增加Redis分布式缓存功能
     *
     * @param entity {@code T}实体类对象
     * @param clazz  {@code T}实体类Class对象
     * @return true表示删除成功
     */
    default boolean removeById(T entity, Class<T> clazz) {
        String key = PlusUtils.customKey(entity, clazz);
        boolean result = removeById(entity);
        RedisUtils.remove(key);
        return result;
    }

    /**
     * 重载{@link IService#removeByIds(Collection)}方法 增加Redis分布式缓存功能
     *
     * @param idList 批主键ID
     * @param clazz  {@code T}实体类Class对象
     * @return true表示删除成功
     */
    default boolean removeByIds(Collection<? extends Serializable> idList, Class<T> clazz) {
        Set<String> keys = EntityUtils.toSet(idList, e -> PlusUtils.customKey(clazz, e));
        boolean result = removeByIds(keys);
        RedisUtils.remove(keys);
        return result;
    }

    /**
     * 重载{@link IService#updateById(Object)}方法 增加Redis分布式缓存功能
     *
     * @param entity DO实体类对象
     * @param clazz  {@code T}实体类Class对象
     * @return true表示更新成功
     */
    default boolean updateById(T entity, Class<T> clazz) {
        String key = PlusUtils.customKey(entity, clazz);
        RedisUtils.remove(key);
        boolean result = updateById(entity);
        RedisUtils.remove(key);
        return result;
    }

    /**
     * 重载{@link IService#getById(Serializable)}方法 增加Redis分布式缓存功能
     *
     * @param id    主键ID
     * @param clazz {@code T}实体类Class对象
     * @return {@code T}实体类对象实例
     */
    default T getById(Serializable id, Class<T> clazz) {
        return PlusUtils.ifNotCache(id, clazz, this::getById);
    }

    /**
     * 重载{@link IService#getById(Serializable)}方法 增加Redis分布式缓存功能
     *
     * @param id 主键ID
     * @param ms 过期时间 毫秒
     * @return {@code T}实体类对象实例
     */
    default T getById(Serializable id, long ms) {
        return PlusUtils.ifNotCache(id, getEntityClass(), ms, this::getById);
    }

    /**
     * 重载{@link IService#getById(Serializable)}方法 增加Redis分布式缓存功能
     *
     * @param id    主键ID
     * @param ms    过期时间 毫秒
     * @param clazz {@code T}实体类Class对象
     * @return {@code T}实体类对象实例
     */
    default T getById(Serializable id, long ms, Class<T> clazz) {
        return PlusUtils.ifNotCache(id, clazz, ms, this::getById);
    }

    /**
     * 重载{@link IService#listByIds(Collection)}方法 增加Redis分布式缓存功能
     *
     * @param idList 批主键ID
     * @param clazz  {@code T}实体类Class对象
     * @return 以{@code T}实体类为元素的集合实例
     */
    default List<T> listByIds(Collection<? extends Serializable> idList, Class<T> clazz) {
        return PlusUtils.ifNotCache(idList, clazz, this::listByIds);
    }

    /**
     * 重载{@link IService#listByIds(Collection)}方法 增加Redis分布式缓存功能
     *
     * @param idList 批主键ID
     * @param ms     过期时间 毫秒
     * @return 以{@code T}实体类为元素的集合实例
     */
    default List<T> listByIds(Collection<? extends Serializable> idList, long ms) {
        return PlusUtils.ifNotCache(idList, getEntityClass(), ms, this::listByIds);
    }

    /**
     * 重载{@link IService#listByIds(Collection)}方法 增加Redis分布式缓存功能
     *
     * @param idList 批主键ID
     * @param clazz  {@code T}实体类Class对象
     * @param ms     过期时间 毫秒
     * @return 以{@code T}实体类为元素的集合实例
     */
    default List<T> listByIds(Collection<? extends Serializable> idList, long ms, Class<T> clazz) {
        return PlusUtils.ifNotCache(idList, clazz, ms, this::listByIds);
    }
}
